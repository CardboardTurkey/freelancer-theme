---
title: Upstream contributions
layout: default
modal-id: 3
img: upstream.svg
alt: image-alt
description: |
  Some highlights of my public contributions.

  <h3> Rust contributions </h3>
  <ul>
    <li>
      <a href="https://github.com/a-maier/hepmc2/pull/1">Add async API to HepMC2</a> (a Physics crate), avoiding code
      duplication using maybe-async.
    </li>
    <li>
      <a href="https://github.com/tjtelan/git-url-parse-rs/pull/55">Fix a panic and improve error handling</a> in
      `git-url-parse` crate.
    </li>
    <li>
      <a href="https://github.com/RonniSkansing/duration-string/pull/6">Add support for multiple formats</a> to
      "duration-string" crate.
    </li>
  </ul>
  <h3> Nix contributions </h3>
  <ul>
    <li>
      <a href="https://github.com/nix-community/home-manager/pull/3156">Add module to nix's home-manager</a> for
      zsh-substring-search
    </li>
    <li>
      <a href="https://github.com/NixOS/nixpkgs/pulls?q=is%3Apr+author%3ACardboardTurkey+is%3Amerged">Various fixes and
        package</a> contributed to nixpkgs.
    </li>
  </ul>
  <h3>Misc</h3>
  <ul>
    <li>
      <a href="https://gitlab.com/qemu-project/qemu/-/commits/master?author=Kiran%20Ostrolenk"> Add vector cryptography
      support</a> to QEMU for RISC-V emulation.
    </li>
  </ul>
---

